---
title-meta: Mixing time of quantum Gibbs samplers
author-meta: Angelo Lucia (Caltech)
geometry: margin=4cm
---

## Mixing time of quantum Gibbs samplers
_Angelo Lucia (Caltech)_

Gibbs samplers are Markovian semigroups whose evolution converges towards the termal equilibrium of a given Hamiltonian (the Gibbs state). The time that the evolution takes, in the worst case, to converge close to the thermal state is known as the mixing time. Knowing the mixing time of a process is useful for many applications, and it can be estimated with various functional inequalities.

In the case of classical spin systems on a lattice, it was shown that it is possible to determine the mixing properties of a semigroup from some "static" clustering condition of the thermal state.
This suggests the question of whether the same is true for quantum spin systems.

In this talk, I will focus on the specific case of Gibbs states of commuting Hamiltonians, and present some recent result in this direction. I will introduce the notion of a
_quantum conditional relative entropy_ and show how it can be used to prove
quasi-factorization (or approximate tensorization) properties of the quantum relative entropy. I will show how these can be then used to bound log-Sobolev constants for product semigroups with heath-bath generators, and under stronger assumptions in more general situations.
