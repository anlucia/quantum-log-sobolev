\documentclass[10pt,compress]{beamer}
\usepackage{appendixnumberbeamer}
\usepackage[utf8]{inputenc}
\usetheme{metropolis}

\usetikzlibrary{patterns, positioning, intersections, calc, arrows, fit, through, fadings, patterns}
\tikzset{onslide/.code args={<#1>#2}{ \only<#1>{\pgfkeysalso{#2}}}}
\tikzfading[name=fade out, inner color=transparent!0, outer color=transparent!100]
\tikzset{
  lattice-site/.style={shape=circle, minimum size=2.0pt, inner sep=0pt, draw=gray, fill=gray},
  error-site/.style={shape=rectangle, minimum size=2.0pt, inner sep=0pt, draw=red, fill=red},
  sigma-z/.style={draw=blue, fill=blue},
  sigma-x/.style={draw=red, fill=red}
}

\newcommand{\columnsbegin}{\begin{columns}}
\newcommand{\columnsend}{\end{columns}}

\usepackage{physics}
\usepackage{dsfont}

\newcommand{\field}[1]{\ensuremath{\mathds{#1}}}
\newcommand{\ring}{\mathds}
\newcommand{\Z}{\ring{Z}}
\newcommand{\CC}{\ring{C}}

\DeclareMathOperator{\supp}{supp}
\DeclareMathOperator{\diam}{diam}
\DeclareMathOperator{\dist}{dist}
\DeclareMathOperator{\Ent}{Ent}
\DeclareMathOperator{\gap}{gap}


\newcommand{\identity}{\mathds{1}}
\newcommand{\bounded}{\ensuremath{\mathcal B}}
\newcommand{\hs}{\ensuremath{\mathcal H}}
\newcommand{\lind}{\ensuremath{\mathcal L}}
\newcommand{\matrixalg}{\ensuremath{\mathcal M}}
\newcommand{\alg}{\ensuremath{\mathcal A}}
\newcommand{\dirichelet}{\ensuremath{\mathcal E}}
\newcommand{\ent}[2]{D\qty(#1\|#2)}
\newcommand{\entA}[3]{D_{#1}\qty(#2\|#3)}
\newcommand{\Expect}{\mathds{E}}

\newcommand{\mcl}{\mathcal}

% fixing appendix translate issues
\pdfstringdefDisableCommands{%
  \def\translate#1{#1}%
}

%%%colors
\definecolor{Green}{HTML}{00AD69}  % "Pantone 3405"
\definecolor{coolblue}{RGB}{0,51,102}
\definecolor{lightblue}{RGB}{102,210,255}
\definecolor{lightpurple}{RGB}{140,30,255}
\definecolor{lightpink}{RGB}{204,0,204}
\definecolor{midblue}{RGB}{0,102,204}
\definecolor{midpink}{RGB}{153,0,153}
\definecolor{darkblue}{RGB}{0,0,153}
\definecolor{cyan}{RGB}{0,204,204}
\definecolor{lightgreen}{RGB}{0,255,128}
\definecolor{midgreen}{RGB}{0,204,0}
\definecolor{midyellow}{RGB}{204,204,0}
\definecolor{darkyellow}{RGB}{153,153,0}
\definecolor{darkpurple}{RGB}{102,0,102}

%%% PICTURES OF SPIN CHAINS
\usepackage{tikz}
\usepackage{ifthen}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes.geometric}

\tikzset{
  % align TikZ pictures in equations
  equation/.style={
    baseline={([yshift=-.5ex]current bounding box.center)},
    scale=0.7
  },
  spin/.style={
    shade, shading=ball, ball color=coolblue
  }
}

\graphicspath{{./figures/}}

\title{Mixing time estimates for Quantum Markov Semigroups}
\author[A. Lucia]{Angelo Lucia (Universidad Complutense de Madrid)
}
\titlegraphic{
  \center
  \includegraphics[height=15pt]{FSE.png}
  \hspace{1.5cm}
  \includegraphics[height=15pt]{MICINN.png}
  \hspace{1.5cm}
  \includegraphics[height=15pt]{UCM.png}
  }
\date{%
SEMINARIO DE ANÁLISIS MATEMÁTICO Y MATEMÁTICA APLICADA -- 5 diciembre 2024
}

\begin{document}
\maketitle

\section{Quantum Markov Semigroups}

\begin{frame}{Quantum systems}

  \begin{block}{Quantum systems}
    \begin{itemize}
    \item a complex Hilbert space $\hs$ (today: finite dimensional)
    \item ``measurable quantities'' (i.e. \emph{observables}) are Hermitian operators in $\bounded(\hs)$
    \item a \emph{state} is a positive normalized functional on $\bounded(\hs)$ which we identify with a \emph{density operator} $\rho \in
      \bounded(\hs)$
      \[
        \bounded(\hs) \ni A \mapsto \expval{A}_\rho = \tr(\rho \, A)
      \]
      \[
       \rho \ge 0, \quad \tr \rho = 1
      \]
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Isolated quantum systems}

  \begin{block}{Evolution (closed systems)}
    Time evolution is given by a unitary group $U(t) = \exp( -i t H)$, where $H\in
    \bounded(\hs)$ is an Hermitian operator known as \emph{Hamiltonian}.
  \end{block}

  State evolution is the solution to Schrödinger equation
  \[ \dv{t} \rho(t) = -i \comm{H}{\rho(t)}, \quad \rho(t) = U_t \rho(0)
    U_t^*. \]

  Equivalently, we can consider the evolution of observables (Hilbert-Schmidt
  dual), in the Heisenberg picture:
  \[
    \dv{t} A(t) = i \comm{H}{A(t)}, \quad A(t) = U_t^* A(0) U_t
    \]
    so that $\expval{A}_{\rho(t)} = \expval{A(t)}_\rho$.


\end{frame}

\begin{frame}{Open systems and completely positive maps}

  In a closed system, energy is conserved: $H(t) = H$ for every $t$.

  \begin{block}{How can we describe non-closed systems?}
    \pause
    We want to map density operators to density operators.

    $T : \bounded(\hs) \to \bounded(\hs)$ is said to be a \alert{quantum
      channel} or \alert{CPTP} if
    \begin{enumerate}
    \item $T$ is linear
    \item $T$ is trace preserving $\tr T(\rho) = \tr \rho$
    \item $T$ is \emph{completely positive}:
      \[ T \otimes \operatorname{id}(\rho) \ge 0 \quad \text{if } 0 \le \rho \in
        \bounded(\hs_1\otimes \hs_2).  \]
    \end{enumerate}
    
  \end{block}
  
\end{frame}


\begin{frame}{Quantum Markov Semigroups}
  
  \begin{block}{QMS}
    A Quantum Markov Semigroup $\{T_t\}_{t\ge 0}$ is a continuous semigroup of CPTP maps:
    \[
      T_0 = \operatorname{id} \qc T_t \circ T_s = T_{t+s}
    \]
    \pause $\{T_t\}_{t\ge 0}$ has a generator $L$, so that $T_t = \exp(tL)$.
  \end{block}
  
  $\rho(t) = T_t(\rho(0))$ is the solution to Liouville-von Neumann equation:
  \[  \dv{t} \rho(t) = L \rho(t) \]

  The assumption that $\{T_t\}$ is a QMS imply that $L$ cannot be an arbitrary
  operator: if $\hs$ is finite-dimensional, it can be expressed in the \emph{Lindblad-Gorini-Kossakowski-Sudarshan form}.
\end{frame}

\begin{frame}{Davies generators}
  Are QMS a good description of certain physical models?

  \begin{exampleblock}{Davies generators}
    In (E. Davies, Comm. Math. Phys. 1974), under certain hypotheses, open systems
    described by a thermal bath at positive temperature are shown to be described
    by a QMS in the \alert{weak-coupling limit}.
  \end{exampleblock}

  \begin{figure}[H]
    \centering
    \begin{tikzpicture}[scale=0.6]
      \node[draw=blue!70, fill=blue!20, shape=circle] (sys) at (0,0) {system};
      \node[draw=green!40, fill=green!40, shape=circle] (env) at (4,0) {environment};
      \draw[-,dashed, line width=2pt, color=green!70] (sys) to (env);
    \end{tikzpicture}
  \end{figure}
  
\end{frame}

\section{Mixing time estimates}

\begin{frame}{Ergodicity}
  % \begin{block}{Quantum channels are contractive}
  %   If $T$ is a CTPT map, then
  %   \[ \norm{T}_{1\to 1} = \norm{T^*}_{\infty \to \infty} = 1 \]
  %   \vspace{-0.5em}
%    (The same holds for the \emph{completely bounded} version of these norms.)
%  \end{block}
%  This implies that the spectrum of $T$ is contained in the unit circle. The
%  spectrum which lies in $\{\abs{z}=1\}$ is denoted the \emph{peripheral
%    spectrum} and can be shown to be composed of simple eigenvalues.

%    \pause
  \begin{block}{QMS are contractive}
    If $L$ is the generator of a QMS, then its spectrum lies in the semiplane
    $\{\Re{z} \le 0\}$.

    \begin{itemize}
    \item $\{ \Re{z} = 0 \}$ correspond to periodic states.
    \item $\ker L$ is the subspace of invariant states
    \end{itemize}
  \end{block}

\begin{exampleblock}{Ergodicity}
  A QMS $\{T_t\}_{t\ge0}$ is \alert{ergodic} if it has a unique positive fixed
  point $\sigma>0$ and    $T_t(\rho) \to \tr(\rho) \sigma$
  as $t\to \infty$ for all $\rho$.
\end{exampleblock}

\end{frame}

\begin{frame}{Mixing time}
  Let $\norm{\cdot}_p$ the $p$-Schatten norm on $\bounded(\hs)$. 
  \begin{block}{Contraction and mixing time}


    The \emph{contraction} of $\{T_t\}_{t\ge0}$ is given by
    \[ \eta(t) = \frac{1}{2} \sup_{\substack{\rho \ge 0 \\ \tr \rho =1}}
      \norm{T_t(\rho) - \sigma}_1 \]
    The \emph{mixing time} is given by
    $ \tau(\epsilon) = \inf \{t \ge 0 \,|\, \eta(t) \le \epsilon \} $
  \end{block}
  How can be get good estimates on $\tau$?
\end{frame}

\begin{frame}{Detailed balance}
  If $\sigma \in \bounded(\hs)$ is positive definite, we can define the
  \emph{GNS scalar product}
  \begin{equation*}
    \braket{A}{B}_{\sigma} = \tr( \sigma A^* B)
  \end{equation*}
  Let $L_2(\sigma) := (\bounded(\hs), \norm{\cdot}_{\sigma})$. 
%  If $T$ is CPTP and $T(\sigma) = \sigma$, then we also have $\norm{T^*}_{\sigma \to \sigma} = 1$.
  \pause
  \begin{block}{Detailed balance}
    We say that $L$ satisfies \alert{detailed balance} w.r.t. $\sigma$ if $L^*:
    L_2(\sigma)\to L_2(\sigma)$ is self-adjoint. In this case
    \begin{itemize}
      \item $\sigma$ is an invariant state for $L$
      \item the spectrum of $L$ is real and negative.
      \end{itemize}
    \end{block}

\end{frame}


\begin{frame}{Spectral gap}
  From now on $\{T_t\}_{t\ge0}$ will be a detailed balance QMS with unique
  invariant state $\sigma$.

  \begin{description}
  \item[Dirichelet form]
    $ \mathcal E(A,B) = \innerproduct{A}{-L^*(B)}_\sigma =
      \innerproduct{-L^*(A)}{B}_\sigma $
  \item[Variance]
    $ Var_{\sigma}(A) = \expval{A^2}_{\sigma} - \expval{A}_\sigma^2 $
  \end{description}
  

  \begin{exampleblock}{Poincaré inequality}
    \[  c Var_{\sigma}(A) \le \mathcal E(A,A) \]
    The optimal constant $c$ is the \alert{spectral gap} of $L$: the smallest
    non-zero absolute value of eigenvalues of $L$.
  \end{exampleblock}
\end{frame}

\begin{frame}{Spectral gap and mixing time}

  \begin{block}{Theorem}
    If $L$ has spectral gap $\lambda$, then
    \[
      \tau(\epsilon) \le \frac{\log(\sigma_{\min}^{-1/2}) - \log \epsilon}{\lambda}
      \]
    \end{block}
\pause
    \begin{block}{Proof sketch}
      $Var_{\sigma}(A(t)) =  \expval{A(t)^2}_{\sigma} - \expval{A(t)}_\sigma^2 =
      \norm{A(t)}_\sigma^2 - \expval{A}_\sigma^2$ and
      \[
        \dv{t} Var_\sigma(A(t)) = -2 \mathcal E(A(t)) \le -2\lambda Var_\sigma(A(t))
      \]
      where $\mathcal E(A) = \mathcal E(A,A)$. So we have
      \[
        Var_\sigma(A(t)) \le
        \norm{A(0)-\expval{A}_\sigma}_{\sigma} e^{-2\lambda t} \le
       2 \norm{A}_\infty \sigma_{\min}^{-1/2} e^{-2\lambda t}.
        \]

    \end{block}

\end{frame}

\begin{frame}{Quantum relative entropy and entropy production}
  \begin{block}{Umegaki's Quantum relative entropy}
    \[\ent{\rho}{\sigma} = \tr \rho(\log\rho-\log\sigma)\]
    Pinsker's inequality:
    $
      \norm{\rho-\sigma}^2_1 \le 2 \ent{\rho}{\sigma}
    $
  \end{block}
  \pause
  We follow the same strategy as in the case of $Var_\sigma$:
  \[
    \dv{t} \ent{\rho(t)}{\sigma} = - \mcl K(\rho(t))
  \]
  where
  $ \mcl K(\rho) = - \tr L(\rho)(\log \rho - \log \sigma)$
    is the \emph{entropy production}.

    \begin{block}{Modified log-Sobolev inequality (MLSI)}
      \[ c\ent{\rho}{\sigma} \le \mcl K(\rho) \]
      the optimal constant $\alpha$ is the (modified-) log-Sobolev constant.
    \end{block}
  \end{frame}


  \begin{frame}{Log-Sobolev inequality and mixing time}

    \begin{Theorem}
      If $\alpha$ is the Log-Sobolev constant of $L$, then
      \[
        \tau(\epsilon) \le \frac{\log\log(\sigma_{\min}^{-1/2}) - \log \epsilon}{\alpha}
      \]
    \end{Theorem}

    We have an exponential improvement in the dependence on $\sigma_{\min}$ over the spectral gap bound!

    \[
      \tau(\epsilon) \le \frac{\log(\sigma_{\min}^{-1/2}) - \log \epsilon}{\lambda}
    \]

\end{frame}

% \begin{frame}{2-log-Sobolev inequality}
%   We can rewrite the 1-log-Soblev inequality as
%   \[
%     2 \alpha_1 \Ent_1(A) \le \mcl E_1(A)
%   \]
%   where
%   \begin{align*}
%     \Ent_1(A) &= \ent{\sigma^{1/2}A \sigma^{1/2}}{\sigma} - \log \expval{A}_\sigma; \\
%     \mcl E_1(A) &= \mcl K(\sigma^{1/2}A\sigma^{1/2}).
%   \end{align*}

%   The classical inequality introduced by Gross corresponds to a 2-log-Sobolev inequality
%   \[
%     2 \alpha_2 \Ent_2(A) \le \mcl E_2(A)
%   \]
%   where
%   \begin{align*}
%     \Ent_2(A) &= \Ent_1(\sigma^{-1/4} A \sigma^{1/2} A \sigma^{-1/4}); \\
%     \mcl E_2(A) &= \mcl E(A,A).
%   \end{align*}


% \end{frame}

% \begin{frame}{1- and 2- log-Sobolev}
%   There is another scalar product we can introduce on $\bounded(\hs)$:
%   \[
%     \braket{A}{B}_{0,\sigma} = \tr \sigma A^*B.
%   \]
%   We say that $L$ satisfies 0-detailed balance if $L^*$ is self-adjoint with respect to
%   $\braket{\cdot}{\cdot}_{0,\sigma}$.

%   \begin{theorem}[Fagnola-Umanità 2008]
%     If $L$ satisfies 0-detailed balance, then it satisfies the standard detailed balance.
%     On the contrary, if $L$ satisfies detailed balance and it commutes with $A
%     \mapsto \sigma A \sigma^{-1}$, then it satisfies the 0-detailed balance.
%   \end{theorem}

%   \begin{theorem}[Kastoryano-Temme 2013, Bardet 2017]
%     If $L$ satisfies the 0-detailed balance, then
%     \[
%       \frac{\lambda}{\log(1/\sigma_{\min})+2} \le \alpha_2 \le \alpha_1 \le \lambda.
%     \]
%   \end{theorem}


% \end{frame}


% \begin{frame}{2-log-Sobolev inequality and Hypercontractivity}
%   The 2-log-Sobolev inequality is related to hypercontractivity of the semigroup.
%   Remember, $T^*_t: L_p(\sigma) \to L_p(\sigma)$ is contractive.

%   \begin{theorem}[Olkiewicz-Zegarlinski 1999]
%     If $L$ satisfies 0-detailed balance, then the following are equivalent
%     \begin{enumerate}
%     \item $L$ satisfies a 2-log-Sobolev inequality
%     \item for $q(t) = 1 + e^{2\alpha t}$ it holds that $\norm{T_t^*}_{\sigma\to (q(t),\sigma)}
%       \le 1 $.
%     \end{enumerate}
%   \end{theorem}

% \end{frame}

\section{Quantum spin systems}

\begin{frame}{Many-body quantum systems}
  Let us now consider \alert{many-body} quantum systems:
  \columnsbegin
    \begin{column}{0.75\textwidth}
  \begin{itemize}[<+->]
  \item $\Gamma$ infinite graph (for example $\Gamma = \Z^D$)
  \item a finite-dimensional Hilbert space $\hs_u$ for each $u\in \Gamma$
    (e.g. $\hs_u = \CC^2$) 
  \item for each $\Lambda \subset \Gamma$ finite, $\hs_{\Lambda} = \bigotimes_{u\in \Lambda}
    \hs_u$
  \item $\mcl A_{\Lambda} = \mcl B(\mcl H_{\Lambda})$
  \item if $\Lambda' \subset \Lambda$ then $\mcl A_{\Lambda'} \hookrightarrow \mcl A_{\Lambda}$ via $O_{\Lambda'} \mapsto O_{\Lambda'}
    \otimes \identity_{\Lambda\setminus \Lambda'}$
    \item the minimal $\Lambda$ such that $O \in \mcl A_{\Lambda}$ is denoted the \emph{support} of
      $O$ and we write $\supp O$.
%      \item $\mcl A_{ql} = \overline{\bigcup_{\Lambda \nearrow \Gamma} \mcl
%          A_{\Lambda}}^{\norm{\cdot}}$ is the $C^*$-algebra of quasi-local observables (the
%        \emph{thermodynamic limit}).
    \end{itemize}
  \end{column}
  \begin{column}{0.3\textwidth}
    \begin{center}
    \begin{tikzpicture}
      \tikzstyle{node}=[shape=circle,draw=blue!70,fill=blue!50,scale=0.5]
      \tikzstyle{smallnode}=[draw=red!60, fill=red!50, scale=0.3]
      \draw[step=.5cm] (-1.4,-1.4) grid (1.4,1.4);
      \only<2>{
        \foreach \x in {-1,-0.5,...,1}
        \foreach \y in {-1,-0.5,...,1}
        {
          \node[smallnode] at (\x,\y) {};
        }
      }
      \only<3-4>{
        \foreach \x in {-0.5,0,...,0.5}
        \foreach \y in {-0.5,0,...,0.5}
        {
          \node[smallnode] at (\x,\y) {};
        }
        \draw[dashed] (-0.75,-0.75) rectangle (0.75,0.75) node {$\Lambda$};
      }
      \only<5->{
       \node (A) at (0,0) {};
       \node (B) at (0.8,0) {};
       \node [draw=blue!70, fill=blue!20, fill opacity=0.4, circle through=(B),label=north:$O$] at (A) {};
       \node[smallnode] at (A) {};
       \foreach \x in {-0.5,0.5} {
         \node[smallnode] at (0,\x) {};
         \node[smallnode] at (\x,0) {};
         \node[smallnode] at (\x,\x) {};
         \node[smallnode] at (\x,-\x) {};
       }
       }
      \end{tikzpicture}
    \end{center}
  \end{column}
  \columnsend

\end{frame}

\begin{frame}{Locality}

\columnsbegin

\begin{column}{6cm}
  For closed systems, we assume that Hamiltonians are \alert{local}
        \[  H_\Lambda = \sum_{u \in \Lambda}   h_u ;\quad \supp  h_u = B_u(r) \]
      $r$ is known as \emph{interaction length}.
    \end{column}

\begin{column}{4cm}
      \begin{tikzpicture}
      \tikzstyle{node}=[shape=circle,draw=blue!70,fill=blue!50,scale=0.5]
      \tikzstyle{smallnode}=[draw=red!60, fill=red!50, scale=0.3]
       \draw[step=.5cm] (-1.4,-1.4) grid (1.4,1.4);
       \node (A) at (0,0) {};
       \node (B) at (0.8,0) {};
       \node [draw=blue!70, fill=blue!20, fill opacity=0.4, circle through=(B),label=north:$h_u$] at (A) {};
       \node[node,label=right:$u$] at (A) {};
       \foreach \x in {-0.5,0.5} {
          \node[smallnode] at (0,\x) {};
          \node[smallnode] at (\x,0) {};
          \node[smallnode] at (\x,\x) {};
          \node[smallnode] at (\x,-\x) {};
        }
      \end{tikzpicture}
    \end{column}

\columnsend

They are \alert{commuting} if
\[ \comm{h_u}{h_v} = 0 \qc \forall u,v \in \Gamma \]

\pause
\begin{exampleblock}{Commuting Hamiltonians are not classical!}
This class is sufficient to describe many (if not all) low-energy features of
quantum systems.
\end{exampleblock}

\end{frame}

\begin{frame}{Thermalization}
  \begin{definition}[Gibbs state -- thermal equilibrium]
    \[ \sigma^{\Lambda,\beta} = \frac{1}{Z_\beta} e^{-\beta H_{\Lambda}} \]
    $\beta < \infty$ is the \emph{inverse temperature}
  \end{definition}
  \pause
  \begin{block}{Thermalization}
    If $H_\Lambda$ is commuting, we can construct QMS which are ergodic with
    $\sigma^{\Lambda,\beta}$ as fixed point, and they are also \alert{local}
    \[
      L_\Lambda = \sum_{u\in \Lambda} L_u \qc \supp L_u = B_u(r')
    \]
    ($r'$ is usually larger than $r$). One example are Davies generators.
  \end{block}
\end{frame}

\begin{frame}{System size scaling: spectral gap vs. log-Sobolev inequality}

  For Gibbs states of local  Hamiltonians, $\log \sigma_{\min}^{-1} \sim
  \order{\abs{\Lambda}}$

  \begin{itemize}
\item    If the spectral gap $\lambda$ of $L_\Lambda$ is lower bounded by a positive
  constant uniformly in $\Lambda$, then
  \[\tau(\epsilon) \sim \log \sigma_{\min}^{-1/2} \sim \order{\abs{\Lambda}} \]
    \pause

  \item If the log-Sobolev constant is  \only<-2>{uniform}\only<3>{\alert{$\log^{-1} \abs{\Lambda}$}} 
  \[
    \tau(\epsilon) \sim \log \log \sigma_{\min}^{-1/2} \sim \order{\log{\abs{\Lambda}}}
  \]

This regime is known as \alert<-2>{rapid mixing}. It implies many strong properties of the evolution:
  for example, stability against perturbations (Cubitt-L.Michalakis-Pérez-García
  2014).
\end{itemize}
\end{frame}

% \section{Lessons from classical systems}

% \begin{frame}{Log-Sobolev constant of product dynamics}
%   Classically, in the case of product dynamics (where the interaction range is
%   0) the log-Sobolev inequality \alert{tensorizes}: it is stable by taking many
%   independent copies of the semigroup.

%   \[
%     \alpha(L_1\otimes \identity \otimes \dots \otimes \identity + \dots +\identity\otimes \dots
%     \otimes \identity \otimes L_n) = \min_{i} \alpha(L_i).
%   \]

%   \begin{alertblock}{Open problem:}
%   In case of non-interacting QMS, is the log-Sobolev constant the minimum of the log-Sobolev
%   constants of the components?
%   \end{alertblock}
% \end{frame}


% \begin{frame}{Lesson learned from classical spin systems}

%   Starting from the seminal work of Martinelli-Olivieri (1994) on the Glauber dynamics, it emerged
%   that for some class of classical spin dynamics there are conditions \emph{on the invariant state}
%   which guarantee a uniform log-Sobolev inequality.

%   \begin{definition}[Mixing condition]
%     A (classical) state $\mu$ satisfies the \emph{mixing condition} if there exists positive
%     constants $C_1$ and $C_2$ such that, for al $A,B \subset
%     \Lambda$ such that $A\cap B = \emptyset$, we have that
%     \[
%       \norm{ \dv{\mu_{A\cup B}}{\mu_{A} \otimes \mu_{B}} -\identity}_\infty \le C_1 e^{-C_2 \dist(A,B)} .
%   \]
%   \end{definition}
% \end{frame}
% \begin{frame}{Log-Sobolev at high temperatures}
% \begin{theorem}[Cesi (2001); Dai Pra-Paganoni-Posta (2002)]
%     If the state $\mu$ satisfies the mixing condition then the corresponding
%     Glauber dynamics satisfies a log-Sobolev inequality.
%   \end{theorem}

%   Moreover, if $\mu$ is a Gibbs state of a (classical) local Hamiltonian at sufficiently high
%   temperatures, or of a 1D system, the mixing condition holds.

%   \begin{alertblock}{Open problem:}
%     For quantum local \alert{commuting} Hamiltonians (which are the only ones for which we know how
%     to define local QMS), does the dynamic preparing high-temperture Gibbs states have a log-Sobolev constant?
%   \end{alertblock}

% \end{frame}

% \begin{frame}[standout]

%   \begin{center}
%     1D SYSTEMS
%     \begin{tikzpicture}[equation]
%       \node at (-1, 0) {$\cdots$};
%       \foreach \n in {1,...,10}{
%       \draw[spin] ({\n-1},0) circle (0.2);
%     }
%      % \node (L) at (5.1,-1) {$\CC^{d}$};
%      % \draw[->] (5,-0.8) -- (5,-0.4);
%       \node at (10, 0) {$\cdots$};
%     \end{tikzpicture}
%   \end{center}

  
% \end{frame}



% \begin{frame}{Overview of the proof}
% We take inspiration from Cesi (2001), Dai Pra, Paganoni, Posta (2002)

% \begin{block}{Geometric decomposition}
% Generators are local, therefore entropy production is too!
% \[ \mathcal{L} = \sum_i \mathcal{L}_i \quad \Rightarrow\quad \mathcal{K}(\rho) = \sum_{i} \mathcal{K}_i(\rho) \qc \mathcal{K}_i(\rho) := -\tr \mathcal{L}_i(\rho)(\log \rho -\log \sigma) \]
% \end{block}

% \begin{alertblock}{Idea:}
% 	Can we similarly decompose $\ent{\rho}{\sigma}$ as a sum of ``local'' terms which depend on a finite region around $i$?
	
% 	\[ \ent{\rho}{\sigma} \le C \sum_{i} \entA{i}{\rho}{\sigma} \quad ? \]
% \end{alertblock}

% \end{frame}

% \begin{frame}{Sketch of the proof}
% \begin{block}{Step 1: Approximate tensorization of the relative entropy}
% \begin{enumerate}
% \item
% $\displaystyle \ent{\rho}{\sigma} \le C \sum_k \ent{\rho}{E^*_{A_k} (\rho)}$ \\
% 	where $E_{A_k}$ is the cond. expectation on invariant sub-algebra of region $A_k$.
% \only<1>{
% \[ \Lambda = \bigcup_k A_k, \quad \abs{A_k} \sim \log(n), \quad \abs{A_k\cap A_{k+1} }\sim \text{const.} \]
% \vspace{-1em}
% \begin{center}
%    \begin{tikzpicture}[scale=0.4]
%     \foreach \n in {1,...,4}{
%       \draw[spin] ({\n-1},0) circle (0.2);
%     }
% \draw [thick,midblue,xshift=-0.5pt,yshift=-0.6pt](-0.5,-0.6) -- (4.5,-0.6) node[black,midway,yshift=-0.7em] { \textcolor{midblue}{$A_1$}};
% \begin{scope}[xshift=4cm]
%       \draw[spin] (0,0) circle (0.2);
% \end{scope}
% \begin{scope}[xshift=5cm]
%     \foreach \n in {1,...,3}{
%       \draw[spin] ({\n-1},0) circle (0.2);
%     }
%     \draw [thick,midpink,xshift=-0.5pt,yshift=-0.6pt](-1.5,0.6) -- (4.2,0.6) node[black,midway,yshift=+0.7em] { \textcolor{midpink}{$A_2$}};
% \end{scope}
% \begin{scope}[xshift=8cm]
%       \draw[spin] (0,0) circle (0.2);
% \end{scope}
% \begin{scope}[xshift=9cm]
%     \foreach \n in {1,...,3}{
%       \draw[spin] ({\n-1},0) circle (0.2);
%     }
%  \draw [thick,midblue,xshift=-0.5pt,yshift=-0.6pt](-0.4,-0.6) -- (3.3,-0.6) node[black,midway,yshift=-0.7em] { \textcolor{midblue}{$A_3$}};
% \end{scope}
% \begin{scope}[xshift=12cm]
%       \draw[spin] (0,0) circle (0.2);
% \end{scope}
% \begin{scope}[xshift=13cm]
%     \foreach \n in {1,...,4}{
%       \draw[spin] ({\n-1},0) circle (0.2);
%     }\draw [thick,midpink,xshift=-0.5pt,yshift=-0.6pt](-1.5,0.6) -- (3.5,0.6) node[black,midway,yshift=+0.7em] { \textcolor{midpink}{$A_4$}};
% \end{scope}
% \end{tikzpicture}
% \end{center}
% }	
% \pause
% \item 
% $\displaystyle \ent{\rho}{E^*_{A_k} (\sigma)} \le k_{A_k} \sum_{i\in A_k} \ent{\rho}{E^*_i(\rho)} $ \\ 1
% where $k_{A_k} \sim \abs{A_k} \log(\lambda)$ where $\lambda$ is the \alert{spectral gap} of the generator in $A_k$ (which we know is constant from Kastoryano-Brandao 2016 )
% \end{enumerate}
% \end{block}
% \pause
% \begin{block}{Step 2: Local MLSI} 
% $\mathcal{L}_i$ acts on a finite system $\Rightarrow$ it always satisfies a (complete) MLSI (Gao-Rouzé arXiv:2102.04146)
% \[ 
% 	\sum_i \ent{\rho}{E^*_i(\rho)} \le \alpha' \sum_i \mathcal{K}_i(\rho) = \alpha' \mathcal{K}(\rho)
% \]
% \end{block}

% \end{frame}

\begin{frame}[standout]

  \begin{center}
    QUANTUM MEMORIES

  \end{center}
  \vfill
  \begin{center}
    \begin{tikzpicture}[scale=0.6]
      \foreach \x in {1,...,5} 
      \foreach \y in {1,...,5}{
        
        \begin{scope}[xshift=\x cm, yshift=\y cm]
          \draw[gray] (0,0)
          -- (-1,0) node[midway, lattice-site] {}
          -- (-1,1) node[midway, lattice-site] {}
          -- (0,1)  node[midway, lattice-site] {}
          -- (0,0)  node[midway, lattice-site] {};
        \end{scope}
      }
    \end{tikzpicture}
  \end{center}

  
\end{frame}

\begin{frame}{Quantum error correcting codes}
  \begin{exampleblock}{Quantum error correcting codes}
    Quantum Hamiltonian $H$ with degenerate ground state space with dimension $d$.

    We can encode a qu$d$it (i.e. a vector in $\mathds{C}^d$) into the ground state space.
  \end{exampleblock}

  \pause

  \begin{block}{Topological memories}
    Orthogonal ground states are \alert{locally indistinguishable} (operators
    with ``small'' support have identical expectation values).
  \end{block}

  \begin{exampleblock}{Quantum double models (Kitaev 1997)}
    A family of topological quantum memories on a 2D square lattice on a torus, indexed by
    finite groups $G$. When $G=\field{Z}_2$, it is known as \emph{toric code}.
  \end{exampleblock}
  
\end{frame}

\begin{frame}{Topological memories}
  An external environment (e.g. thermal bath) can create errors:

        \begin{center}
        \begin{tikzpicture}[scale=0.6]
          \foreach \x in {1,...,5} 
          \foreach \y in {1,...,5}{
            
            \begin{scope}[xshift=\x cm, yshift=\y cm]
              \draw[gray] (0,0)
              -- (-1,0) node[midway, lattice-site] {}
              -- (-1,1) node[midway, lattice-site] {}
              -- (0,1)  node[midway, lattice-site] {}
              -- (0,0)  node[midway, lattice-site] {};
            \end{scope}
          }

          \only<2-6>{
            \node[sigma-x, error-site] (A) at (1,3) {};
          }
          \only<2>{
            \draw[blue, thick] (A)
            -- ++(0.5,0) node[lattice-site, sigma-z] {}
            -- ++(0.5,0) node[sigma-x, error-site]  {};                       
          }
          \only<3>{
            \draw[blue, thick] (A)
            -- ++(0.5,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(0.5,0) node[sigma-x, error-site]  {};                       
          }
          \only<4>{
            \draw[blue, thick] (A)
            -- ++(0.5,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(0.5,0) node[sigma-x, error-site]  {};                       
          }
          \only<5>{
            \draw[blue, thick] (A)
            -- ++(0.5,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(0.5,0) node[sigma-x, error-site]  {};                       
          }
          \only<6>{
            \draw[blue, thick] (A)
            -- ++(0.5,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(0.5,0);

            \draw[blue, thick] (0,3)
            node[sigma-x, error-site]  {};                       
          }
          \only<7->{
            \draw[blue, thick] (A)
            -- ++(0.5,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(1,0) node[lattice-site, sigma-z] {}
            -- ++(0.5,0);

            \draw[blue, thick] (0,3)
            -- ++(0.5,0) node[lattice-site, sigma-z] {}
            -- ++(1,0);
          }

                    
        \end{tikzpicture}
      \end{center}


\uncover<8->{
  
  \begin{description}
  \item[Active error correction:] Check for the presence of error, correct them
    before encoded information is damaged!
  \item[Self-correction:] The physical properties of the Hamiltonian protect the
    information: its life-time improves (exponentially) with the system size $N$.
    We can make a better memory just by taking larger systems!
  \end{description}
}
\end{frame}

\begin{frame}{Self-correction and spectral gap}

  \begin{block}{Scaling?}
    \begin{itemize}
    \item If $\gap(L) \to 0$ (for large enough $\beta$) then we can have \emph{self-correction}.
    \item If $\gap(L) \ge \lambda > 0$ as system size $N$ grows, then
      we have \emph{no self-correction}; but $\tau(\epsilon)\sim \abs{\Lambda}$
    \item If $\alpha(L) \ge \log^{-1}\abs{\Lambda}$ then we also have
      $\tau(\epsilon)\sim\log \abs{\Lambda}$.
    \end{itemize}
  \end{block}
  \pause
  
  \begin{exampleblock}{In classical systems}
    For non-quantum spin systems:
    \begin{itemize}
    \item 1D Ising model is \emph{not} self-correcting;
    \item 2D Ising model is \emph{self-correcting}: existence of a critical
      temperature and phase transition.
    \end{itemize}
    Moreover, spectral gap and log-Sobolev inequality are equivalent in the classical setting!
  \end{exampleblock}

  \pause[\thebeamerpauses]
  \begin{center}
    \alert{Can we find self-correcting quantum memories?}
  \end{center}
  
\end{frame}

\begin{frame}{Self-correcting quantum memories}
  \begin{center}
    YES! but in 4D!    
  \end{center}
  
  \begin{exampleblock}{4D toric code}    
    The 4D Toric Code has a gapless Davies generator and is a self-correcting
    quantum memory.
    \[ \gap(L) = \order{e^{-c N}} \quad \text{for large $\beta$.} \]
    (Dennis, Kitev, Landahl, Preskill 2002, Alicki, Horodecki, Horodecki, Horodecki 2010)
  \end{exampleblock}

  \begin{center}
    \alert{Can we find 1D, 2D or 3D self-correcting quantum memories?}
  \end{center}

\end{frame}

\begin{frame}{The state of the art - 1D systems}
  No pretense of exhaustivity!
  \begin{block}{Classical models}
    \begin{itemize}
    \item Holley, Stroock 1989: log-Sobolev inequality for 1D classical spin chains (Glauber dynamics) : $\alpha\sim \log^{-1}(n)$
    \item Zegarlinski 1990: $\alpha \sim $ constant
    \end{itemize}
  \end{block}
  \begin{block}{Quantum models (Davies generators)}
    \begin{itemize}
    \item Alicki, Fannes, Horodecki 2009: spectral gap quantum ferromagnetic 1D Ising model% (Davies generators)
    \item Kastoryano, Brandao 2016: spectral gap for 1D commuting quantum spin chains %(Davies generators)
%    \item Capel, Rouzé, Stilck França (arXiv:2009.11817): log-Sobolev at high temperature (Schmidt generators)
    \end{itemize}
  \end{block}    
  
\end{frame}


\begin{frame}{Our contribution - 1D systems}

\begin{exampleblock}{\small Bardet, Capel, Gao L. Pérez García, Rouzé. CMP 2024}
For a commuting 1D spin chain Hamiltonian, the associated ergodic Davies generator at any inverse temperature $\beta < \infty$ satisfies a MLSI with constant
\[ \alpha(L) \ge C \log^{-1}(n) \]

When $\beta \to \infty$, $\alpha \sim \exp(-c' \beta)$.
\end{exampleblock}

\pause

The proof relies on the fact that the Davies generator has a uniform spectral
gap (as shown by Kastoryano and Brandao in 2016).

% Consequently, the mixing time is bounded by
% \[ \tau \le C'  \operatorname{polylog}(n) \]


\end{frame}


\begin{frame}{State of the art - 2D systems}

  \begin{itemize}[<+->]
  \item The 2D Toric Code is not self-correcting (Alicki, Fannes, Horodecki 2008):
  \[ \gap(L) = \Omega( e^{-c \beta}) \quad \text{uniform in  $N$.} \]
  \item 2D quantum double models for abelian groups are not self-correcting (Kómár, Landon-Cardinal,
    Temme 2016)
  \end{itemize}
\end{frame}

\begin{frame}{Our contribution - 2D systems}
  \begin{exampleblock}{L., Pérez-García, Pérez-Hernández. Forum of Mathematics, Sigma, 2023}
    For any quantum double model with arbitrary group $G$, we have that
    \[
      \gap(L) \ge\lambda(\beta, G) >0
    \]
    In particular, they are not self-correcting.
  \end{exampleblock}
  \pause
%\end{frame}
%\begin{frame}{Proof's ideas}
  The proof is based on \alert{tensor network techniques}.
  \begin{enumerate}
  \item $\sigma^{\beta}$ is a Projected Entangled Pair State (PEPS)
  \item PEPS have a natural frustration-free Hamiltonian associated to them,
    called the \alert{parent Hamiltonian} $H^{\text{parent}}$.
  \item We can control the gap of $L$ gap with the one of $H^{\text{parent}}$:
    \[ \gap(L) \ge C \gap(H^{\text{parent}}) \]
  \item We can use tools specific for spectral gaps of parent Hamtiltonians:
    Kastoryano, L., Perez-Garcia Commun. Math. Phys. (2019).
  \end{enumerate}
\end{frame}

\begin{frame}{Overlook}

  \begin{itemize}[<+->]
    \item 1D quantum spin systems with commuting interactions always thermalize
      rapidly (log-Sobolev constant for Davies generators)
    \item Quantum double models in 2D are  not self-correcting, since their
      thermal process has a spectral gap at every temperature.
  \end{itemize} 
  \pause[\thebeamerpauses]
  \textbf{Future work:}
  \begin{itemize}[<+->]
  \item Heuristic arguments indicate that no commuting 2D model should be self
    correcting. Can we make this rigorous? A first example: quantum double models with Hopf algebra replacing
    group algebra.
  \item Better mixing time estimates in 2D: log-Sobolev inequality for 2D quantum double models.
  \item Self-correcting memories in 3D?
  \end{itemize}

  \pause[\thebeamerpauses]
  \begin{center}
    \textbf{Thank you for your attention!}
  \end{center}
  
\end{frame}

   \end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
